# lpGBT-RD53A-BERT
Python-based library for running BERT tests on IT portcard set-ups (basis of procedures implemented for IT portcards in Ph2ACF). 
Repo forked from S.Wertz's original repo : https://gitlab.cern.ch/swertz/lpgbt-rd53a-bert. 

## Requirements

- USB driver and adafruit libraries for I2C communication: see https://github.com/swertz/Adafruit_Python_GPIO/tree/lpGBT for install instructions
- Cactus (IPbus)
- Either a compiled `Ph2_ACF` (https://gitlab.cern.ch/cms_tk_ph2/Ph2_ACF) or Vivado 2017.4
- This project: `git clone https://gitlab.cern.ch/swertz/lpgbt-rd53a-bert.git`
- Python packages: `future`, `matplotlib`, `numpy`.

## Setting up

- Turn on FC7, load the firmware `fc7_top_optical_10G_v0_X.bit` (either using Ph2_ACF's `fpgaconfig` or using Vivado and Xilinx JTAG programmer) (if needed, start RARP: `sudo /usr/sbin/rarpd -a`), where the version `X` is:

| `X` | Optical (IC) config | Sync word | PRBS checker | Uplink frame printout | Aurora readout |
| :---------: | :---------------: | :-----------: | :---------: | :---------: | :---------: |
| 13 | :heavy_check_mark: | `0xCCCC` (40MHz clock) | :heavy_check_mark: | :heavy_check_mark: | |
| 10 |                    | `0xCCCC` (40MHz clock) | :heavy_check_mark: | | |
| 9  |                    | `0xAAAA` (80MHz clock) | :heavy_check_mark: | | |

- Setup the environment: `cd lpgbt-rd53a-bert; source ./setup.sh` 
- Configure the firmware:
    - Make sure IP address at the top of `libpyDTC/fc7_daq.py` is the one of the FC7
    - Run `./libpyDTC/fc7_daq.py cfg` (configure clocks)
    - Run `./libpyDTC/fc7_daq.py rst` (reset)
- Make sure the DIP switches on the portcard are `11011100` (nr. 1-8) when using I2C, and `11010100` when using IC.
- Power up portcard (2.5V, max. 1A)
- If using the I2C interface:
    - Run `./configLpGBT.py -i i2c --pll`. You should reach status "13".
    - Run `./configLpGBT -i i2c --dll`. You should reach status "18".
- If using the IC interface:
    - Run `./configLpGBT -i ic --dll`. You should reach status "18".
- Enable required up- and downlink groups: e.g. `./configLpGBT.py -i INTERFACE -c --up 1 --down 0`, where `INTERFACE` is `i2c` or `ic`. See "More information" below for which groups to use. **Warning**: in some cases the elink polarity has to be inverted (assuming you've respected the *physical* elink polarities - see "More information" below):
    - "portcard MCX/SMA <-> green board <-> DP" (SCC or TEPX module): `--invert-down`
    - "portcard elink <-> yellow board <-> SMA <-> green board <-> DP" (SCC or TEPX module): `--invert-down`
    - "portcard elink <-> Dominik's mini-DP <-> DP" (SCC or TEPX module): `--invert-up`
    - Plain elink (TBPX module): nothing
- If you intend to use the TBPX module, do not forget to turn on the chiller (2 buttons)!!
- Power up SCC (1.8V, max. 1A) or module (e.g. 2.5V/6.5A).
- Initialize the chips: `./libpyDTC/fc7_daq.py init` (disregard the error at the end)
- Configure RD53A: e.g. `./configRD53A.py --lanes 0 1 --data 2 0 --enable-taps 1 --invert-taps 1`. This enables the CML driver on lanes 0 and 1, and sends PRBS on lane 0 and clock on lane 1. It also enables and inverts TAP1, which corresponds to 1-tap pre-emphasis, and is likely the only mode we'll ever use. You can keep TAP1 disabled, but then be sure to fix `TAP1=0` when running the scans (below).
- If the loaded firmware supports it (v>=13), try `./libpyDTC/fc7_daq.py read -u 1` to read one raw uplink frame on uplink group 1.

## Running a BERT scan
 
- Run `./scanBERT.py -v scan --interface ic --mode lpgbt --up 0 --time 10 --fix TAP1=50,TAP2=0 --connection DP -o ./DATE_CC_test`. This will:
    - Use the IC channel for lpGBT config.
    - Use the lpGBT internal pattern checker. Using the FC7 is also possible, in that case do `--mode fpga`. Warning: in fpga mode, if no data arrives to the FC7 the test will not fail and will report 0 BER.
    - Check for PRBS7 on elink input group 0 of lpGBT (be sure you're sending a PRBS from RD53A on that channel, see previous section!).
    - Scan sampling phase and equalization settings of specified input group in lpGBT and scan TAP0 in RD53A, wile keeping TAP1 and TAP2 fixed (warning: if you don't fix anything, you'll start a 5D scan which can take a long time!)
    - Run a BERT for each setting using `2**(5 + 2 * 10) * 32` bits
    - Store the results into `2019-11-07_14-12_DP_test.json` (date/time will change)
- Run `./scanBERT.py plot -i ./2019-11-07_14-12_DP_test.json -o ./DATE_CC_test --params PHASE EQ --fix TAP0=500`. This will make a 2D plot of the results with the phase on the x axis and the equalization setting on the y axis, using the data taken with TAP0 set to 500, and save the plot as `2019-11-07_14-12_DP_test_ber.png`. Warning: if you don't specify a value for TAP0, a different 2D plot will be produced for each scanned value!

The scripts are capable of scanning over all 5 parameters (phase, EQ, TAP0-2), or to fix any of them to a specific value, and produce 1D or 2D plots from the data. For an n-dimensional plot, if more than n parameters are scanned, you can fix the values of the other parameters. If you don't, for 1D plots this will draw a different line on the same plot for all the scanned values of the non-fixed parameters, while for 2D plots this will produce a different plot for all the scanned values of the non-fixed parameters.

## More information

- Check out the various options of the above commands: `./scanBERT.py CMD --help` (where `CMD` is one of `scan`, `plot` -> also try simply `./scanBERT.py --help`)
- We have the following correspondence for lpGBT groups, connector groups, yellow board (YB) connectors, green/Bonn board, and RD53A data lanes:

| lpGBT group | Portcard connector | YB connector (P/N) | Bonn board | RD53A lanes (SCC) |
| :---------: | :---------------: | :-----------: | :---------: | :---------: |
| down 0      | J6 (Cmd0)         | J5/J6         | aux         | cmd         |
| up 3        | J6 (d0)           | J4/J2         | 0-3         | 3-0         |
| up 1        | J6 (d1)           | J3/J1         | 0-3         | 3-0         |
| up 2        | J6 (d2)           |               |             |             |
| up 0        | J6 (d3)           |               |             |             |
| down 2      | J5 (Cmd1)         | J5/J6         | aux         | cmd         |
| up 4        | J5 (d4)           | J4/J2         | 0-3         | 3-0         |
| up 5        | J5 (d5)           | J3/J1         | 0-3         | 3-0         |
| down 3      | MCX               |               |             |             |
| up 6        | MCX               |               |             |             |


- Note that the TP elinks should be connected with "Ted" convention on both ends when using the yellow board (i.e. paddle board writing "up"), where "up" for the portcard means the side with the black pastic blocker. For portcard-module, use "Ted" on portcard side and "Malte" (writing down) on the HDI side. If you're not sure about the pin assigments, check this out: https://gist.github.com/swertz/824337b8a1339467c71abdce0c5a5d95
- You can use `./configLpGBT.py` and `./configRD53A.py` to configure the chips (see the `--help`), or use `lpGBT_tools.py` and `RD53A_tools.py` interactively: either run e.g. `ipython -i lpGBT_tools.py`, or enter `from lpGBT_tools import * ` in a python console (use the `help()` on any of the functions below).
- lpGBT examples:
    - First to `lp = LpGBT("i2c")`, if using the I2C interface
    - `lp.configLPGBT()`: load minimal config and startup
    - `lp.configDownLink(3, invert=True)`: enable downlink group3/channel0, inverted polarity
    - `lp.configUpLink(6, equal=0, phase=7)`: enable uplink goup6/channel0, specify equalization level and sampling phase
    - `lp.configBERT(6)`: configure pattern checker to test for PRBS7 from uplink group 6
    - `lp.runBERT(11, verbose=True)`: run BERT with `2**(5 + 2 * 11) * 32` bits
    - `modeUpLink`, `modeDownLink`: change up-/downlink data source (data/constant pattern/PRBS7/loopback)
    - `disableUpLink`, `disableDownLink`
    - `setBERTPattern`, `setDPPattern`
    - `findPhase`
- RD53A examples:
    - `configureCML(0)`: enable uplink lane 0, enable/disable and possibly invert pre-empasis taps (by default we enable and invert TAP1, corresponding to 2-tap pre-emphasis)
    - `sendClock(3)`: send 640 Mbps clock signal on lane 3
    - `sendPRBS7(3)`: send PRBS7 on lane 3
    - `setTapX` (with X=0,1,2): set TAP level (0-1023, defaults at startup are 500/0/0)
- The scan ranges are hardcoded in `scanBERT.py`, if needed changed them there.
- Using Vivado debug:
    - To launch vivado (on `b186pixlab` account, simply run `~/vivado.sh`):
    ```
    export XILINXD_LICENSE_FILE=2112@lxlicen01,2112@lxlicen02,2112@lxlicen03
    source /opt/Xilinx/Vivado/2017.4/settings64.sh
    vivado &
    ```

    - Open hardware manager, auto-connect to target
    - Debug core: load the `.ltx` file if not already done
    - Add all probe pins
    - `rx_data` is the data received on the uplink. It has the form `00_G6_..._G0`, where each of `GX` is a 32-bit (8-digit) hex number corresponding to the data received by lpGBT on uplink group X.
    - `tx_data` is the data sent on the downlink. It has eight hex numbers, each of which is sent on one of the 8 possible lpGBT downlink channels (4 groups, 2 channels/group when running at 160 Mbps). When idling each channel sends the downlink sync patter (which will depend on the firmware version).
    - By setting `tx_sel` to `1` you can send the pattern of `lpgbt_tx_data_vio` on the downlink.
    - Resetting can sometimes help if the signal is lost (`reset_gbtbank_from_vio` to `1` and back to `0`).
    - The compiled firmware and debug core are in `/home/swertz/Documents/Yiannis/IT_uDTC_optical_10G_v0_X/`

## IC channel and efuses

To use the IC (optical serial) channel as documented above, you need to have the fuses blown. In any case, make sure you use the correct firmware version.

### No fuses burnt

If the fuses have not been blown yet it's still possible to test the IC channel:

- Make sure pin 5 of the DIP switch on the portcard is ON
- Configure the firmware (`cfg` and `init` commands)
- Power up the portcard
- Run `/configLpGBT.py -i i2c --pll`. This initializes the PLL in the lpGBT to recover the clock from the downlink and decode IC commands.
- Switch pin 5 OFF.
- Run `./configLpGBT.py -i ic --dll`. This finalizes the initialization of the lpGBT.
- You can now run the usual commands, turn on eports, run BERTs, etc, but note that `configLpGBT` must now always be called **with** `-i ic`.

### Burning the fuses

To burn the minimal configuration to the fuses (that which is loaded over I2C when running `--pll` in the previous step):
- Make sure the power supply can be controlled from the scripts: try `./powerSupply.py CHAN CMD`, where `CHAN` is `1` (right channel) or `2` (left channel), and `CMD` can be e.g. `on`, `off`, `readV`.
- The lpGBT must be connected to channel `1`.
- Channel `2` must be turned OFF and connected to the lower pin of the J11 pair (next to the J5 elink connector). Make sure both channels have a common ground.
- Run the `cfg` and `rst` commands for the FC7.
- Make sure pin 5 of the DIP switch is ON.
- Run `./blowFuses.py --real burn` and follow the instructions.
- After it's done, disconnect the J13 pin and test the result: run `./blowFuses.py --real test` and follow the instructions.
